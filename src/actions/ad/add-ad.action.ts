import { APIGatewayProxyHandler } from 'aws-lambda';
import { AdService } from '@service/ad.service';
import { IAd } from '@beans/IAd';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
const validations = require("@validators/ad/create.validator.json")

const addAd: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    console.log("Adding new Ad",event.body);
    
    const requestData = JSON.parse(event.body)
  
    return validateAgainstConstraints(requestData,validations)
            .then(async ()=>{
              let ad = await AdService.addAd(requestData)
              return ad
            }) 
    // return  UserService.addUser(requestData) // if you want to test without constraints umcomment this and comment the above lines
            .then((ad:IAd)=>{
              response = new ResponseModel({ad},200,"Ad successfully added")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Ad could not be added');
            })
            .then(()=>{
              return response.generate()
            })
  }

export const main = addAd;