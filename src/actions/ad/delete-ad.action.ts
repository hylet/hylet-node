import { APIGatewayProxyHandler } from 'aws-lambda';
import { AdService } from '@service/ad.service';
import { IAd } from '@beans/IAd';
import ResponseModel from '@models/response.model';

const deleteAd: APIGatewayProxyHandler =async (event) => {
    let response;
    let adId = event.pathParameters.id;
    console.log("Deleting Ad with id "+adId);
  
    return AdService.deleteAd(adId)
      .then((ad:IAd)=>{
        response = new ResponseModel({ad},200,"Deleted the ad")
      })
      .catch((error)=>{
        console.log(error)
        response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Ad could not be Deleted');
      })
      .then(()=>{
        return response.generate()
      })
  }

export const main = deleteAd;