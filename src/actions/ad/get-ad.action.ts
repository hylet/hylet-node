import { APIGatewayProxyHandler } from 'aws-lambda';
import { AdService } from '@service/ad.service';
import { IAd } from '@beans/IAd';
import ResponseModel from '@models/response.model';

const getAd: APIGatewayProxyHandler =async (event) => {
    let response;
    let adId = event.pathParameters.id;
    console.log("Getting Ad with id "+adId);
  
    return AdService.getAd(adId)
      .then((ad:IAd)=>{
        response = new ResponseModel({ad},200,"Returned the ad")
      })
      .catch((error)=>{
        console.log(error)
        response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Ad could not be retreived');
      })
      .then(()=>{
        return response.generate()
      })
  }

export const main = getAd;