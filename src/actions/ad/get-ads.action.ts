import { APIGatewayProxyHandler } from 'aws-lambda';
import { AdService } from '@service/ad.service';
import { IAd } from '@beans/IAd';
import ResponseModel from '@models/response.model';


const getAds: APIGatewayProxyHandler =async () => {
    let response;
    console.log("Getting all the Ads");
  
    return AdService.getAds()
      .then((ads:IAd[])=>{
        response = new ResponseModel({ads},200,"Returned all the ads")
      })
      .catch((error)=>{
        console.log(error)
        response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Ads could not be retreived');
      })
      .then(()=>{
        return response.generate()
      })
  
  }

export const main = getAds