import { APIGatewayProxyHandler } from 'aws-lambda';
import { AdService } from '@service/ad.service';
import { IAd } from '@beans/IAd';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
const validations = require("@validators/ad/update.validator.json")

const updateAd: APIGatewayProxyHandler  = async (event) => {
  
  let response;
  let id = event.pathParameters.id;
  console.log("Updating ad with id ",id);
  const requestData = JSON.parse(event.body)
  requestData.id = id;
  
  return validateAgainstConstraints(requestData,validations)
          .then(async ()=>{
            let ad = await AdService.updateAd(id,requestData)
            return ad
          }) 
          .then((ad:IAd)=>{
            response = new ResponseModel({ad},200,"Successfully updated Ad")
          })
          .catch((error)=>{
            console.log(error)
            response = (error instanceof ResponseModel) ? error : new ResponseModel({error}, 500, 'Ad could not be updated');
          })
          .then(()=>{
            return response.generate()
          })
}

export const main = updateAd;