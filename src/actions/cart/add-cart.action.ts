import { APIGatewayProxyHandler } from 'aws-lambda';
import { CartService } from '@service/cart.service';
import { ICart } from '@beans/ICart';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
const validations = require("@validators/cart/create.validator.json")

export const main: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    console.log("Adding new Cart",event.body);
    
    const requestData = JSON.parse(event.body)
  
    return validateAgainstConstraints(requestData,validations)
            .then(async ()=>{
              let cart = await CartService.addCart(requestData)
              return cart
            }) 
    // return  UserService.addUser(requestData) // if you want to test without constraints umcomment this and comment the above lines
            .then((cart:ICart)=>{
              response = new ResponseModel({cart},200,"Cart successfully added")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Cart could not be added');
            })
            .then(()=>{
              return response.generate()
            })
  }
