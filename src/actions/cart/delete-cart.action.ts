import { APIGatewayProxyHandler } from 'aws-lambda';
import { CartService } from '@service/cart.service';
import { ICart } from '@beans/ICart';
import ResponseModel from '@models/response.model';


export const main: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    let id = event.pathParameters.id;
    console.log("Deleting cart with id ",id);
    
    return CartService.deleteCart(id)
            .then((cart:ICart)=>{
              response = new ResponseModel({cart},200,"Successfully cart deleted")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({error}, 500, 'Card cloud not be deleted');
            })
            .then(()=>{
              return response.generate()
            })
  }

 