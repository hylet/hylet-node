import { APIGatewayProxyHandler } from 'aws-lambda';
import { CartService } from '@service/cart.service';
import { ICart } from '@beans/ICart';
import ResponseModel from '@models/response.model';

export const main: APIGatewayProxyHandler =async (event) => {
    let response;
    let id = event.pathParameters.id;
    console.log("Getting Cart with id "+id);
  
    return CartService.getCart(id)
      .then((cart:ICart)=>{
        response = new ResponseModel({cart},200,"Returned the cart")
      })
      .catch((error)=>{
        console.log(error)
        response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Cart could not be retreived');
      })
      .then(()=>{
        return response.generate()
      })
  }