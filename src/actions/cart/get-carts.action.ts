import { APIGatewayProxyHandler } from 'aws-lambda';
import { CartService } from '@service/cart.service';
import { ICart } from '@beans/ICart';
import ResponseModel from '@models/response.model';


export const main: APIGatewayProxyHandler =async () => {
    let response;
    console.log("Getting all the Carts");
  
    return CartService.getCarts()
      .then((carts:ICart[])=>{
        response = new ResponseModel({carts},200,"Returned all the carts")
      })
      .catch((error)=>{
        console.log(error)
        response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Carts could not be retreived');
      })
      .then(()=>{
        return response.generate()
      })
  
  }