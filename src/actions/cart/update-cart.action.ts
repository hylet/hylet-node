import { APIGatewayProxyHandler } from 'aws-lambda';
import { CartService } from '@service/cart.service';
import { ICart } from '@beans/ICart';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
const validations = require("@validators/cart/update.validator.json")

export const main: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    let id = event.pathParameters.id;
    console.log("Updating cart with id ",id);
    const requestData = JSON.parse(event.body)
    requestData.id = id;
    
    return validateAgainstConstraints(requestData,validations)
            .then(async ()=>{
              let cart = await CartService.updateCart(id,requestData)
              return cart
            }) 
            .then((cart:ICart)=>{
              response = new ResponseModel({cart},200,"Successfully updated cart")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({error}, 500, 'Cart could not be updated');
            })
            .then(()=>{
              return response.generate()
            })
  }