import { APIGatewayProxyHandler } from 'aws-lambda';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
import { HandleService } from '@service/handle.service';
import { IHandle } from '@beans/IHandle';
const validations = require("@validators/handle/create.validator.json")

export const main: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    console.log("Adding new Handle",event.body);
    
    const requestData = JSON.parse(event.body)
  
    return validateAgainstConstraints(requestData,validations)
            .then(async ()=>{
              let handle = await HandleService.addHandle(requestData)
              return handle
            }) 
            .then((handle:IHandle)=>{
              response = new ResponseModel({handle},200,"Handle successfully added")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Handle could not be added');
            })
            .then(()=>{
              return response.generate()
            })
  }

