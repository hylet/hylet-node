import { APIGatewayProxyHandler } from 'aws-lambda';
import ResponseModel from '@models/response.model';
import { HandleService } from '@service/handle.service';
import { IHandle } from '@beans/IHandle';

export const main: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    let id = event.pathParameters.id;
    console.log("Getting handle with id ",id);
    
    return HandleService.getHandle(id)
            .then((handle:IHandle)=>{
              response = new ResponseModel({handle},200,"Successfully fetched handle")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({error}, 500, 'Handle could not be fetched');
            })
            .then(()=>{
              return response.generate()
            })
  }

 