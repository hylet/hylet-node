import { APIGatewayProxyHandler } from 'aws-lambda';
import ResponseModel from '@models/response.model';
import { HandleService } from '@service/handle.service';
import { IHandle } from '@beans/IHandle';

export const main: APIGatewayProxyHandler  = async () => {
  
    let response;

    
    return HandleService.getHandles()
            .then((handles:IHandle[])=>{
              response = new ResponseModel({handles},200,"Successfully fetched handles")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({error}, 500, 'Handles could not be fetched');
            })
            .then(()=>{
              return response.generate()
            })
  }

 