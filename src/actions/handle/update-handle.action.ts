import { APIGatewayProxyHandler } from 'aws-lambda';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
import { HandleService } from '@service/handle.service';
import { IHandle } from '@beans/IHandle';
const validations = require("@validators/handle/create.validator.json")

export const main: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    let id = event.pathParameters.id;
    console.log("Updating handle with id ",id);
    
    const requestData = JSON.parse(event.body)
  
    return validateAgainstConstraints(requestData,validations)
            .then(async ()=>{
              let handle = await HandleService.updateHandle(id,requestData)
              return handle
            }) 
            .then((handle:IHandle)=>{
              response = new ResponseModel({handle},200,"Handle successfully updated")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Handle could not be updated');
            })
            .then(()=>{
              return response.generate()
            })
  }

