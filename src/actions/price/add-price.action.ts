import { APIGatewayProxyHandler } from 'aws-lambda';
import { PriceService } from '@service/price.service';
import { IPrice } from '@beans/IPrice';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
const validations = require("@validators/price/create.validator.json")

const addPrice: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    console.log("Adding new Price",event.body);
    
    const requestData = JSON.parse(event.body)
  
    return validateAgainstConstraints(requestData,validations)
            .then(async ()=>{
              let price = await PriceService.addPrice(requestData)
              return price
            }) 
    // return  UserService.addUser(requestData) // if you want to test without constraints umcomment this and comment the above lines
            .then((price:IPrice)=>{
              response = new ResponseModel({price},200,"Price successfully added")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Price could not be added');
            })
            .then(()=>{
              return response.generate()
            })
  }

export const main = addPrice;