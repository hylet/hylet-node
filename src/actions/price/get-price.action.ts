import { APIGatewayProxyHandler } from 'aws-lambda';
import { PriceService } from '@service/price.service';
import { IPrice } from '@beans/IPrice';
import ResponseModel from '@models/response.model';


export const main: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    let id = event.pathParameters.id;
    console.log("Getting price with id ",id);
    
    return PriceService.getPrice(id)
            .then((price:IPrice)=>{
              response = new ResponseModel({price},200,"Successfully fetched price")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({error}, 500, 'Price could not be fetched');
            })
            .then(()=>{
              return response.generate()
            })
  }

 