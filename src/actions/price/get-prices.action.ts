import { APIGatewayProxyHandler } from 'aws-lambda';
import { PriceService } from '@service/price.service';
import { IPrice } from '@beans/IPrice';
import ResponseModel from '@models/response.model';


const getPrices: APIGatewayProxyHandler  = async () => {
  
    let response;
    console.log("Getting all prices");
    
    return PriceService.getPrices()
            .then((prices:IPrice[])=>{
              response = new ResponseModel({prices},200,"List of prices")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({}, 500, 'Prices could not be fetched');
            })
            .then(()=>{
              return response.generate()
            })
  }

export const main = getPrices;