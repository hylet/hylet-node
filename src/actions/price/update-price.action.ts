import { APIGatewayProxyHandler } from 'aws-lambda';
import { PriceService } from '@service/price.service';
import { IPrice } from '@beans/IPrice';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
const validations = require("@validators/price/update.validator.json")

export const main: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    let id = event.pathParameters.id;
    console.log("Updating price with id ",id);
    const requestData = JSON.parse(event.body)
    requestData.id = id;
    
    return validateAgainstConstraints(requestData,validations)
            .then(async ()=>{
              let price = await PriceService.updatePrice(id,requestData)
              return price
            }) 
            .then((price:IPrice)=>{
              response = new ResponseModel({price},200,"Successfully updated price")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({error}, 500, 'Price could not be updated');
            })
            .then(()=>{
              return response.generate()
            })
  }

 