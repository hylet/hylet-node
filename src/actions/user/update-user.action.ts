import { APIGatewayProxyHandler } from 'aws-lambda';
import { UserService } from '@service/user.service';
import { IUser } from '@beans/IUser';
import ResponseModel from '@models/response.model';
import { validateAgainstConstraints } from '@libs/validate';
const validations = require("@validators/user/update.validator.json")

const updateUser: APIGatewayProxyHandler  = async (event) => {
  
    let response;
    let id = event.pathParameters.id;
    console.log("Updating user with id ",id);
    const requestData = JSON.parse(event.body)
    requestData.id = id;
    
    return validateAgainstConstraints(requestData,validations)
            .then(async ()=>{
              let cart = await UserService.updateUser(id,requestData)
              return cart
            }) 
            .then((user:IUser)=>{
              response = new ResponseModel({user},200,"Successfully updated user")
            })
            .catch((error)=>{
              console.log(error)
              response = (error instanceof ResponseModel) ? error : new ResponseModel({error}, 500, 'User could not be updated');
            })
            .then(()=>{
              return response.generate()
            })
  }

export const main = updateUser;