export interface IAd{
    adId:String;
    adName:String;
    adDesc:String;
    userId:String;
    postCaption:String;
    websiteLink:String;
    profileLink:String;
}