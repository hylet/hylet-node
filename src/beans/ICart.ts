export interface ICart {
    cartId: string;
    adId: string;
    handleId: string;
    price: Number;
    duration: string;
    type: string;
    paymentStatus: string;
    choice: string;
    verified: string;
    proof: string;
    rejectReason: string;
}