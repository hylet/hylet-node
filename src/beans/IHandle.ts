export interface IHandle{
    handleId:String;
    userId:String;
    handleName:String;
    displayName:String;
    accountType:String;
    followers:String;
    genre:String;
    picURL:String;
    status:String;
    handleBio:String;
    verified:String;
}