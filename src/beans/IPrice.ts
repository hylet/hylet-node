export interface IPrice {
    priceId:String;
    handleId:String;
    price:Number;
    duration:String;
    type:String;
}
