export interface IUser {
    userId: string;
    name: string;
    email: string;
    password: string;
    mobile: string;
    accType: string;
}
