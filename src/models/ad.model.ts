import { Schema, model } from 'mongoose';
import { IAd } from 'src/beans/IAd';

const schema = new Schema<IAd>({
    adId: { type: String, required: true },
    adName: { type: String, required: true },
    adDesc: {type: String, required: true},
    userId: {type: String, required: true},
    postCaption: {type: String, required: true},
    websiteLink: {type: String, required: true},
    profileLink: {type: String, required: true}
});

export const AdModel = model<IAd>('Ad', schema);
  