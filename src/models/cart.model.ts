import { Schema, model } from 'mongoose';
import { ICart } from 'src/beans/ICart';

const schema = new Schema<ICart>({
cartId: { type: String, required: true},
adId: { type: String, required: true },
handleId: { type: String, required: true },
price: {type: Number, required: true},
duration: {type: String, required: true},
type: {type: String, required: true},
paymentStatus: {type: String, required: true},
choice: {type: String, required: true},
verified: {type: String, required: true},
proof: {type: String, required: true},
rejectReason: {type: String, required: true}
});

export const CartModel = model<ICart>('Cart', schema);