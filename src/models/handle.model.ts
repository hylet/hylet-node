import { Schema, model } from 'mongoose';
import { IHandle } from '@beans/Ihandle';

const schema = new Schema<IHandle>({
    handleId:{type:String,required:true},
    userId:{type:String},
    handleName:{type:String,required:true},
    verified:{type:String},
    displayName:{type:String,required:true},
    accountType:{type:String,required:true},
    followers:{type:String,required:true},
    genre:{type:String,required:true},
    picURL:{type:String},
    status:{type:String},
    handleBio:{type:String},
})

export const HandleModel = model<IHandle>('Handle', schema);