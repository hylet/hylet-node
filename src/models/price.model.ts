import { Schema, model } from 'mongoose';
import { IPrice } from 'src/beans/IPrice';

const schema = new Schema<IPrice>({
    priceId: { type: String, required: true },
    price: { type: Number, required: true },
    handleId: {type: String, required: true},
    duration: {type: String, required: true},
    type: {type: String, required: true}
});

export const PriceModel = model<IPrice>('Price', schema);