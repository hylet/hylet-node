import { Schema, model } from 'mongoose';
import { IUser } from 'src/beans/IUser';

const schema = new Schema<IUser>({
userId: { type: String, required: true},
name: { type: String, required: true },
email: { type: String, required: true },
password: {type: String, required: true},
mobile: {type: String, required: false},
accType: {type: String, required: true,enum:['CC','BO']}
});

export const UserModel = model<IUser>('User', schema);
  