import { AdModel } from "@models/ad.model";
import { Api } from "src/decorators/api.decorator";
import { IAd } from "@beans/IAd";
import { v4 as uuidv4 } from 'uuid';

export class AdService{
    @Api
    static addAd(ad:IAd){
        return new Promise(async(resolve,reject)=>{
            try{
                ad.adId = uuidv4();
                let temp = new AdModel(ad);
                await temp.save()
                resolve(temp)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getAds(){      
        return new Promise(async(resolve,reject)=>{
            try{
                let ads = await AdModel.find({});
                resolve(ads)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getAd(adId:String){
        return new Promise(async(resolve,reject)=>{
            try{
                let ads = await AdModel.find({adId:adId});
                if(ads.length == 0){
                    reject("No such entry")
                }else{
                    resolve(ads[0])
                }
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static updateAd(adId:String,ad:IAd){
        return new Promise(async(resolve,reject)=>{
            try {
                let res = await AdModel.updateOne({adId:adId},ad)
                if(res.modifiedCount == 0){
                    reject("No such entry")
                }else{
                    resolve(ad)
                }
            } catch (error) {
                reject(error)
            }
        })
    }

    @Api
    static deleteAd(adId:String){
        return new Promise(async(resolve,reject)=>{
            try{
                let res = await AdModel.deleteOne({adId:adId})
                if(res.deletedCount == 0){
                    reject("Entry could not be deleted")
                }else{
                    resolve(adId)
                }
            }catch(error){
                reject(error)
            }
        })
    }
}