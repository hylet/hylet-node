import { CartModel } from "@models/cart.model";
import { Api } from "src/decorators/api.decorator";
import { ICart } from "@beans/ICart";
import { v4 as uuidv4 } from 'uuid';

export class CartService{
    @Api
    static addCart(cart:ICart){
        return new Promise(async(resolve,reject)=>{
            try{
                cart.cartId = uuidv4();
                let temp = new CartModel(cart);
                await temp.save()
                resolve(temp)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getCarts(){      
        return new Promise(async(resolve,reject)=>{
            try{
                let carts = await CartModel.find({});
                resolve(carts)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getCart(cartId:String){
        return new Promise(async(resolve,reject)=>{
            try{
                let carts = await CartModel.find({cartId:cartId});
                if(carts.length == 0){
                    reject("No such entry")
                }else{
                    resolve(carts[0])
                }
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static updateCart(cartId:String,cart:ICart){
        return new Promise(async(resolve,reject)=>{
            try {
                let res = await CartModel.updateOne({cartId:cartId},cart)
                if(res.modifiedCount == 0){
                    reject("No such entry")
                }else{
                    resolve(cart)
                }
            } catch (error) {
                reject(error)
            }
        })
    }

    @Api
    static deleteCart(cartId:String){
        return new Promise(async(resolve,reject)=>{
            try{
                let res = await CartModel.deleteOne({cartId:cartId})
                if(res.deletedCount == 0){
                    reject("Entry could not be deleted")
                }else{
                    resolve(cartId)
                }
            }catch(error){
                reject(error)
            }
        })
    }
}