import { v4 as uuidv4 } from 'uuid';
import { Api } from "src/decorators/api.decorator";
import { HandleModel } from '@models/handle.model';
import { IHandle } from '@beans/Ihandle';

export class HandleService{
    @Api
    static addHandle(handle:IHandle){
        return new Promise(async(resolve,reject)=>{
            try{
                handle.handleId = uuidv4();;
                let temp = new HandleModel(handle);
                await temp.save()
                resolve(temp)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getHandles(){      
        return new Promise(async(resolve,reject)=>{
            try{
                let handles = await HandleModel.find({});
                resolve(handles)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getHandle(handleId:String){
        return new Promise(async(resolve,reject)=>{
            try{
                let handles = await HandleModel.find({handleId:handleId});
                if(handles.length > 0){
                    resolve(handles[0])
                }else{
                    reject("No such handle")
                }
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static updateHandle(handleId:String,handle:IHandle){
        return new Promise(async(resolve,reject)=>{
            try {
                let res = await HandleModel.updateOne({handleId:handleId},handle)
                if(res.modifiedCount == 0){
                    reject("No such entry")
                }else{
                    resolve(handle)
                }
            } catch (error) {
                reject(error)
            }
        })
    }

    @Api
    static deleteHandle(handleId:String){
        return new Promise(async(resolve,reject)=>{
            try {
                let res =await HandleModel.deleteOne({handleId:handleId})
                if(res.deletedCount == 0){
                    reject("No such entry")
                }else{
                    resolve(handleId)
                }
            } catch (error) {
                reject(error)
            }
        })
    }
}