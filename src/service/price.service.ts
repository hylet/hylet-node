import { PriceModel } from "@models/price.model";
import { Api } from "src/decorators/api.decorator";
import { IPrice } from "@beans/IPrice";
import { v4 as uuidv4 } from 'uuid';

export class PriceService{
    @Api
    static addPrice(price:IPrice){
        return new Promise(async(resolve,reject)=>{
            try{
                price.priceId = uuidv4();;
                let temp = new PriceModel(price);
                await temp.save()
                resolve(temp)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getPrices(){      
        return new Promise(async(resolve,reject)=>{
            try{
                let prices = await PriceModel.find({});
                resolve(prices)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getPrice(priceId:String){
        return new Promise(async(resolve,reject)=>{
            try{
                let prices = await PriceModel.find({priceId:priceId});
                if(prices.length > 0){
                    resolve(prices[0])
                }else{
                    reject("No such price")
                }
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static updatePrice(priceId:String,price:IPrice){
        return new Promise(async(resolve,reject)=>{
            try {
                let res = await PriceModel.updateOne({priceId:priceId},price)
                if(res.modifiedCount == 0){
                    reject("No such entry")
                }else{
                    resolve(price)
                }
            } catch (error) {
                reject(error)
            }
        })
    }

    @Api
    static deletePrice(priceId:String){
        return new Promise(async(resolve,reject)=>{
            try {
                let res =await PriceModel.deleteOne({priceId:priceId})
                if(res.deletedCount == 0){
                    reject("No such entry")
                }else{
                    resolve(priceId)
                }
            } catch (error) {
                reject(error)
            }
        })
    }
}