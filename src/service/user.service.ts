import { UserModel } from "@models/user.model";
import { Api } from "src/decorators/api.decorator";
import { IUser } from "@beans/IUser";
import { v4 as uuidv4 } from 'uuid';

export class UserService{
    @Api
    static addUser(user:IUser){
        return new Promise(async(resolve,reject)=>{
            try{
                user.userId = uuidv4();
                let temp = new UserModel(user);
                await temp.save()
                resolve(temp)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getUsers(){      
        return new Promise(async(resolve,reject)=>{
            try{
                let users = await UserModel.find({});
                resolve(users)
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static getUser(userId:String){
        return new Promise(async(resolve,reject)=>{
            try{
                let users = await UserModel.find({userId:userId});
                if(users.length == 0){
                    reject("No such entry")
                }else{
                    resolve(users[0])
                }
            }catch(error){
                reject(error)
            }
        })
    }

    @Api
    static updateUser(userId:String,user:IUser){
        return new Promise(async(resolve,reject)=>{
            try {
                let res = await UserModel.updateOne({userId:userId},user)
                if(res.modifiedCount == 0){
                    reject("No such entry")
                }else{
                    resolve(user)
                }
            } catch (error) {
                reject(error)
            }
        })
    }

    @Api
    static deleteUser(userId:String){
        return new Promise(async(resolve,reject)=>{
            try{
                let res = await UserModel.deleteOne({userId:userId})
                if(res.deletedCount == 0){
                    reject("Entry could not be deleted")
                }else{
                    resolve(userId)
                }
            }catch(error){
                reject(error)
            }
        })
    }
}