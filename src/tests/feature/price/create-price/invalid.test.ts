import * as chai from 'chai';
import { handler } from '@test/libs/actions/handler';

// type Response = {
//     data: any;
//     message: string;
//     status: string;
// }

const expect = chai.expect;


const requestData1 =     {
    "handleId":"12345",
    // price cannot be string
    "price":"123",
    "duration":"1 hr",
    // also missing 'type' property
}

describe('POST /price - Creat Price (Invalid)',()=>{
   
    let response, statusCode;

    before(function (done) {
        
        handler.createPrice(requestData1)
            .then((body) => {
                statusCode = 200;
                response = body;
                done();
            })
            .catch((error) => {
                statusCode = error.status;
                response = error.data;
                done();
            });
    });

    it('should expect a 400 status code', (done) => {
        expect(statusCode).to.eql(400);
        done();
    });

    it('should expect a required fields are missing', (done) => {
        expect(response.message).to.eql('required fields are missing');
        done();
    });

})

