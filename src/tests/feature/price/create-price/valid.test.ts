
import * as chai from 'chai';
import { handler } from '@test/libs/actions/handler';


type Response = {
    data: any;
    message: string;
    status: string;
}

const expect = chai.expect;


const requestData =     {
    "handleId":"12345",
    "price":123,
    "duration":"1 hr",
    "type":"post"
}

describe('POST /price - Creat Price',()=>{
   
    let response, statusCode;

    before(function (done) {
        
        handler.createPrice(requestData)
            .then((body) => {
                statusCode = 200;
                response = body;
                done();
            })
            .catch((error) => {
                statusCode = error.response.statusCode;
                response = error.response.body;
                done();
            });
    });

    it('should expect a 200 status code', (done) => {
        expect(statusCode).to.eql(200);
        done();
    });

    it('should expect a success message', (done) => {
        expect(response.message).to.eql('Price successfully added');
        done();
    });

    // check if the data exists in db
    it('should check that data exists in db',()=>{
        handler.getPrice({id:response.priceId})
            .then((res:Response)=>{
                expect(response.data.price.price).to.eql(res.data.price.price);
                expect(response.data.price.handleId).to.eql(res.data.price.handleId);
                expect(response.data.price.duration).to.eql(res.data.price.duration);
                expect(response.data.price.type).to.eql(res.data.price.type);
            })
    })
})

