import axios from "axios";
import type { Method } from "axios";
// const BASE_URL = process.env.BASE_URL;
const BASE_URL = "http://localhost:3000";

const post = (reqType:Method, url = '',data = {}) => {
    return new Promise((resolve, reject) => {
        return axios({
            url:BASE_URL+url,
            method:reqType,
            data:data
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch((error) => {
            reject(error.response);
        })
    })
}

export const handler =   {
    createPrice: (data)=> post("POST","/price",data),
    getPrice:(data)=> post("GET",`/price/${data.id}`,{})
}
