import * as chai from 'chai';
import { UserService } from '@service/user.service';

const userMock = {
    "name":"Eren Edgar",
    "email":"madtitan@gmail.com",
    "password":"mikasaakerman"
}

const expect = chai.expect;
  
describe('Service - User Service',()=>{
    describe('Checking Add User',()=>{
        it("should create a new user record",async ()=>{
            let res:any = await  UserService.addUser(userMock);
            expect(res.name).to.eql(userMock.name)
            expect(res.email).to.eql(userMock.email)
            expect(res.password).to.eql(userMock.password)
            expect(res).to.have.property('_id')
            expect(res._id).not.null
        })
    })
})