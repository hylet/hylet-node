var exec = require('child_process').exec;
let src = process.env.npm_config_src
let cmd = `node node_modules/mocha/bin/mocha \"src/tests/feature/${src}/**/*.test.ts\" --require ts-node/register`
exec(cmd,(error, stdout, stderr)=>{
    if(error){
        console.log(error);
    }else if(stderr){
        console.log(stderr)
    }else{
        console.log(stdout)
    }
})
 // to run resource specific test , use : npm run test-only --src=rsrc_name